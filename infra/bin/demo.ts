import { App } from 'aws-cdk-lib';
import { DemoAppStack } from '../lib/my-demo-app';

const app = new App();
new DemoAppStack(app, 'DemoAppStack', {});

app.synth();
