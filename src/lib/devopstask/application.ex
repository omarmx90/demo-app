defmodule Devopstask.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      DevopstaskWeb.Telemetry,
      {DNSCluster, query: Application.get_env(:devopstask, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Devopstask.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Devopstask.Finch},
      # Start a worker by calling: Devopstask.Worker.start_link(arg)
      # {Devopstask.Worker, arg},
      # Start to serve requests, typically the last entry
      DevopstaskWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Devopstask.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    DevopstaskWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
