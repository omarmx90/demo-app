import { Stack, StackProps, CfnOutput } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as ecs from 'aws-cdk-lib/aws-ecs';
import * as ecr from 'aws-cdk-lib/aws-ecr';
import * as assets from 'aws-cdk-lib/aws-ecr-assets';
import * as ecsPatterns from 'aws-cdk-lib/aws-ecs-patterns';

export class DemoAppStack extends Stack {
  public readonly ecrRepository: ecr.IRepository;

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'MyVpc', {
      maxAzs: 3 
    });

    const dockerImage = new assets.DockerImageAsset(this, 'DemoAppDockerImage', {
      directory: '../src', 
    });

    const cluster = new ecs.Cluster(this, 'EcsCluster', {
      vpc,
    });

    const fargateService = new ecsPatterns.ApplicationLoadBalancedFargateService(this, 'MyFargateService', {
      cluster,
      taskImageOptions: {
        image: ecs.ContainerImage.fromDockerImageAsset(dockerImage), 
        containerPort: 4000, 
        environment: { 
          SECRET_KEY_BASE: 'ZVGhClTTKA9paCXVK2Lyl36DDpp9ujkbaRCoaFBVuzp8dM5FEcWQdBnJZNHl8v86'
        }
      },
      publicLoadBalancer: true
    });

    new CfnOutput(this, 'LoadBalancerURL', { value: fargateService.loadBalancer.loadBalancerDnsName });
    new CfnOutput(this, 'DemoAppDockerImageUri', { value: dockerImage.imageUri });
  }
}
